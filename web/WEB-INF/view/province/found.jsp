<%-- 
    Document   : update
    Created on : 23-nov-2016, 21:18:37
    Author     : usuario
--%>

<%@page import="model.Province"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<jsp:useBean id="province" class="Province" scope="request"/>  

<div id="content">        
    <h1>Localizar provincia</h1>
    <ul>
        <li><%= province.getId() %></li>
        <li><%= province.getProvincia() %></li>
    </ul>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>