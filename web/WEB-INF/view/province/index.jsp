<%-- 
    Document   : index.jsp
    Created on : 22-nov-2016, 0:31:29
    Author     : Rafa
--%>

<%@page import="model.Province"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Lista de provincias</h1>
    
<!--    
    <% 
        if (null != request.getAttribute("countDelete")) {
            Integer count = (Integer) request.getAttribute("countDelete");
            out.println("Borrado: " + count + " registro(s)" );
        };
    
    %>
        -->
        
    
    <jsp:useBean id="provinces" class="ArrayList<model.Province>" scope="request"/>  
    
    <p><a href="<%= request.getContextPath()%>/province/seek">Buscar Provincia</a></p>

    <table>
        <tr>
            <th>Id</th>
            <th>Provincia</th>
            <th>Acción</th>
        </tr>
    <%
        Iterator<model.Province> iterator = provinces.iterator();
        while (iterator.hasNext()) {
            Province province = iterator.next();%>
    <tr>
        <td><%= province.getId()%></td>
        <td><%= province.getProvincia() %></td>
        <td><a href="<%= request.getContextPath()%>/province/remember/<%= province.getProvincia()%>">Recordar</a> 
        </td>
    </tr>
    <%
        }
    %>
    </table>
    
    <a href="<%= request.getContextPath()%>/province/index/1"> 1 </a> -
    <a href="<%= request.getContextPath()%>/province/index/2"> 2 </a> -
    <a href="<%= request.getContextPath()%>/province/index/3"> 3 </a> 

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>