<%-- 
    Document   : index.jsp
    Created on : 22-nov-2016, 0:31:29
    Author     : Rafa
--%>

<%@page import="model.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Lista de usuarios</h1>
    
    <% 
        if (null != request.getAttribute("countDelete")) {
            Integer count = (Integer) request.getAttribute("countDelete");
            out.println("Borrado: " + count + " registro(s)" );
        };
    
    %>
        
        
    
    <p><a href="<%= request.getContextPath()%>/user/new">Alta de usuario</a></p>
    <jsp:useBean id="users" class="ArrayList<model.User>" scope="request"/>  

    <table>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Login</th>
            <th>Operación</th>
        </tr>
    <%
        Iterator<model.User> iterator = users.iterator();
        while (iterator.hasNext()) {
            User user = iterator.next();%>
    <tr>
        <td><%= user.getName() %></td>
        <td><%= user.getSurname() %></td>
        <td><%= user.getLogin() %></td>
        <td><a href="<%= request.getContextPath()%>/user/edit/<%= user.getId()%>">Editar</a>
            <a href="<%= request.getContextPath()%>/user/delete/<%= user.getId()%>">Borrar</a>
        </td>
   
    </tr>
    <%
        }
    %>
    </table>

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>