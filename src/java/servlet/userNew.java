/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import persistence.UserDAO;

/**
 *
 * @author usuario
 */
public class userNew extends HttpServlet {
    UserDAO userDao = new UserDAO();
    private static final Logger LOG = Logger.getLogger(userNew.class.getName());

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.        
        LOG.info("Iniciandodo el servlet");
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User user = null;
        if (request.getMethod().equalsIgnoreCase("post")){
            user = new User(0, 
                request.getParameter("name"),
                request.getParameter("surname"),
                request.getParameter("login"),
                ""); 
            LOG.info("Recogidos datos del formulario");
        } else {
            user = new User();
            user.setName("anonimo");
            LOG.info("GET. Si datos del formulario");
        }
        userDao.connect();
        int count = userDao.insert(user);
        userDao.disconnect();
        
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<h1>Alta de usuarios</h1>");
//            out.println(user + "<br>");
//            out.println("Registros afectados: " + records);
//            

//            
//        }
            //añadir información al request para pasarla a la vista
            request.setAttribute("count", count);
            request.setAttribute("user", user);
            
            //reenviar request a la vista (JSP)
            String nextJSP = "/WEB-INF/view/user/insert.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            //Si reenvio la petición a un servlet sería:
            //RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("nombreServlet");
            //ahora reenviamos:
            dispatcher.forward(request,response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
