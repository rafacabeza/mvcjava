/*
 * Java Bean User
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Rafa
 */
public class Province implements Serializable
{
    private int id;
    private String provincia;

    public Province() {
        this.id = 0;
        this.provincia = "";
    }

    @Override
    public String toString() {
        return "Provincia{" + "id=" + id + ", provincia=" + provincia + '}';
    }

    
    public Province(int id, String name, String surname, String login, String password) {
        this.id = id;
        this.provincia = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    
    
    
}
