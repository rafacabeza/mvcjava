/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class UserJdbcServlet extends HttpServlet {

    
    
    
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private Statement statment = null;
    private Connection connection = null;
    private final String CLASS_DRIVER ="com.mysql.jdbc.Driver";
//    private final String URL ="jdbc:mysql://10.2.25.100/mvc";
    private final String URL ="jdbc:mysql://10.0.2.15/mvc";
    private static final Logger LOG = Logger.getLogger(UserJdbcServlet.class.getName());
    
    @Override
    public void init(ServletConfig config) {
//
//       try {
//           // This block configure the logger with handler and formatter
//           FileHandler fh = new FileHandler("java%u.log", true);
//           LOG.addHandler(fh);
//           LOG.setLevel(Level.INFO);
//           SimpleFormatter formatter = new SimpleFormatter();
//           fh.setFormatter(formatter);
//           // the following statement is used to log any messages
//           LOG.log(Level.WARNING,"My first log");
//       } catch (SecurityException e) {
//           e.printStackTrace();
//       } catch (IOException e) {
//           e.printStackTrace();
//       }        
//        
                
        try {
            Class.forName(this.CLASS_DRIVER);
            connection = DriverManager.getConnection(
                    this.URL,"usuario", "usuario");
            statment = connection.createStatement();

            
            if (connection != null) {
                    LOG.info("Conexión establecida!");
            } else {
                    LOG.severe("Fallo de conexión!");
            }            


        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
    
    @Override
    public void destroy() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "Conexión a la base de datos no cerrada", ex);
        }        
    }
  
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserJdbcServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserJdbcServlet at " + request.getContextPath() + "</h1>");
            
            
            
            out.println("<h1>Lista de las Usuarios:</h1>");
            out.println("<ul>");

            String query = null;
            query = "select * from user";
            ResultSet resultSet = null;
            try {
                synchronized(statment){
                 resultSet = statment.executeQuery(query);
                }

                
                while (resultSet.next()) {
                    out.println("<li>" + resultSet.getString("name")
                            + " " + resultSet.getString("surname") 
                            + " (" + resultSet.getString("login") + ")</li>");
                }
            }
            catch (SQLException ex) {
//                gestionarErrorEnConsultaSQL(ex,  request, response);
                out.println("<p>Errorr SQL</p>");
            }
            finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException ex) {
                        LOG.log(Level.SEVERE,
                                "No se pudo cerrar el Resulset", ex);
                    }
                }
            }
            out.println("</ul>");            
            
            
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }


    
    
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
